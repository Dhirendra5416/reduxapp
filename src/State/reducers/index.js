import { combineReducers } from "redux";

import amountReduces from "./amountReduces";

const reducers = combineReducers({
    amount:amountReduces
})

export default reducers;