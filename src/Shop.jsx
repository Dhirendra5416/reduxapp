import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux';
import { actionCreators } from './State/index'

const Shop = () => {
    const dispatch = useDispatch();
    //this is shows how to send data from one component to another components
    const balance = useSelector(state => state.amount)

    // this is shows destructuring in javascript(reactjs)
    const{ withdrawMoney,depositMoney} = bindActionCreators(actionCreators,dispatch);

    return (
        <div>
            <h2>Deposite/Withdraw Money</h2>
            {/* <button className='btn-primary mx-2' onClick={()=>{dispatch(actionCreators.withdrawMoney(100))}}>-</button>
            Update Money
            <button className='btn-primary mx-2' onClick={()=>{dispatch(actionCreators.depositMoney(100))}}>+</button> */}
            <button className='btn-primary mx-2' onClick={()=>{withdrawMoney(100)}}>-</button>
            Update Money({balance})
            <button className='btn-primary mx-2' onClick={()=>{depositMoney(100)}}>+</button>

        </div>
    )
}

export default Shop